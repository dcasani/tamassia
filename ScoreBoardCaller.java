package com.casani.tamassia.chapter3;

public class ScoreBoardCaller {

	public static void main(String[] args) {
		ScoreBoard board = new ScoreBoard(5);
		board.add(new GameEntry("Daniel", 45));
		board.printBoard();
		board.add(new GameEntry("Maria", 5));
		board.printBoard();
		board.add(new GameEntry("Hubert", 88));
		board.printBoard();
		board.add(new GameEntry("Gloria", 77));
		board.printBoard();
		board.add(new GameEntry("Laura", 11));
		board.printBoard();
		board.add(new GameEntry("Carlos", 90));
		board.printBoard();
		board.remove(4);
		board.printBoard();
		board.remove(0);
		board.printBoard();
		board.remove(8);
		board.printBoard();
	}

}
